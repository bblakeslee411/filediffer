# FileDiffer -- A simple Python3-based text file bulk differ

## Description
FileDiffer will identify all text files in two directories with any differences, based on a provided list of filenames.

## Installation
Required dependencies can be installed with `pip3 install -r requirements.txt`.

FileDiffer depends on texttable and tqdm.

## Usage
FileDiffer can be invoked by `python3 fileDiffer.py --dir1 [/first/dir/to/diff] --dir2 [/second/dir/to/diff] --fileList [fileName]`.

* `--dir1`: First directory of files to examine.
* `--dir2`: Second directory of files to examine.
* `--fileList`: File containing list of files to examine.  The directory structure of `--dir1` and `--dir2` must be the same.
