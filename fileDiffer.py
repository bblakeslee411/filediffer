import argparse
import filecmp
import os
import texttable
import tqdm

BAR_WIDTH = 80


def main():
    args = buildArgParser()
    data = readFileList(args.fileList)
    results = compareFiles(args.dir1, args.dir2, data)
    printResults(results)


def buildArgParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dir1', type=str, required=True,
                        help='First directory to compare.')
    parser.add_argument('--dir2', type=str, required=True,
                        help='Second directory to compare.')
    parser.add_argument('--fileList', type=str, required=True,
                        help='File containing list of file to check.')
    args = parser.parse_args()
    return args


def readFileList(fileList):
    with open(fileList, 'r') as f:
        data = f.read().splitlines()
    return data


def compareFiles(dir1, dir2, fileList):
    results = list()
    for fl in tqdm.tqdm(fileList, desc='Comparing Files', ncols=BAR_WIDTH):
        file1Path = os.path.join(dir1, fl)
        file2Path = os.path.join(dir2, fl)
        same = filecmp.cmp(file1Path, file2Path, shallow=False)
        results.append((fl, same))
    return results


def printResults(results):
    table = texttable.Texttable()
    table.header(('File', 'Same'))
    for r in results:
        if r[1]:
            same = 'True'
        else:
            same = 'False'
        table.add_row((r[0], same))
    print(table.draw())


if __name__ == '__main__':
    main()
